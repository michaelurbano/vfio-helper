#!/bin/bash
# Helpful to read output when debugging
set -x

# Stop display manager
systemctl stop display-manager.service
killall gdm-x-session # Run this regardless of whether or not PC uses GDM, simply makes things easier for variety of Linux distros, all it does is complain a little

# Added from original script, simply unloads nvidia driver modules
sudo rmmod nvidia_drm
sudo rmmod nvidia_uvm
sudo rmmod nvidia_modeset
sudo rmmod nvidia

# Unloads amd driver module
sudo rmmod amdgpu

# Unloads intel driver module
sudo rmmod i915

# Unbind VTconsoles
echo 0 > /sys/class/vtconsole/vtcon0/bind
echo 0 > /sys/class/vtconsole/vtcon1/bind

# Unbind EFI-Framebuffer
echo efi-framebuffer.0 > /sys/bus/platform/drivers/efi-framebuffer/unbind

# Avoid a Race condition by waiting 2 seconds. This can be calibrated to be shorter or longer if required for your system
sleep 2

# Unbind the GPU from display driver
# These must be set by vfiohelper.py based on PCIe addresses found on systemCheck(), in order
# DO NOT MODIFY ANY OF THE LINES WHICH SAY THE WORD BELOW. THEY ARE NEEDED SO THAT VFIO HELPER CAN CORRECTLY DISCONNECT THE GPU

#REPLACE
#REPLACE
#REPLACE
#REPLACE
#REPLACE
#REPLACE
#REPLACE
#REPLACE
#REPLACE
#REPLACE