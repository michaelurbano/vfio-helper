# Credit
There wasn't a license attached to these scripts, so credit goes to Joseph Knockenhauer, and all others that he listed below
***
### Joseph Knockenhauer
All credit goes to Joseph for creating the scripts used here.
[joeknock90 on GitHub](https://github.com/joeknock90)
### [Created the Single-GPU-Passthrough guide](https://github.com/joeknock90/Single-GPU-Passthrough.git)
#### [Fuel my coffee addiction or help me test new hardware](https://www.paypal.com/donate?business=87AQBT5TGFRJS&item_name=Github+Testing&currency_code=USD)
#### Always appreciated, never required.

## Special Thanks to:
### [The Passthrough post](https://passthroughpo.st)
For hosting news and information about VFIO passthrough, and for the libvirt/qemu hook helper in this guide.

### andre-ritcher
For providing the vfio-pci-bind tool. A tool that is no longer used in this guide, but was previously used and he still deserves thanks.

### Matoking
For the Nvidia ROM patcher. Making passing the boot gpu to the VM without GPU bios problems.
Patching the rom is no longer required but I never would have written this guide without the original work so I'm keeping them here. 

### Sporif
For diagnosing, developing, and testing methods to successfully rebind the EFI-Framebuffer when passing the video card back to the host OS.

### droidman
For instructions on manually editing the vBIOS hex for use with VFIO passthrough

### [Yuri Alek](https://gitlab.com/YuriAlek/vfio)
A guide that is no doubt better than mine. Learning a few things from his implementation that can help me out a bit. This guide depends on libvirt at the base where as his has implementations that do not.