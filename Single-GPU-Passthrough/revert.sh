#!/bin/bash
set -x
  
# Re-Bind GPU to Nvidia Driver
# These must be set by vfiohelper.py based on PCIe addresses found on systemCheck(), in inverse order
# DO NOT MODIFY ANY OF THE LINES WHICH SAY THE WORD BELOW. THEY ARE NEEDED SO THAT VFIO HELPER CAN CORRECTLY RECONNECT THE GPU

#REPLACE
#REPLACE
#REPLACE
#REPLACE
#REPLACE
#REPLACE
#REPLACE
#REPLACE
#REPLACE
#REPLACE

# Reload nvidia modules
modprobe nvidia
modprobe nvidia_modeset
modprobe nvidia_uvm
modprobe nvidia_drm
# Reload amd module
modprobe amdgpu
# Reload intel module
modprobe i915

# Rebind VT consoles
echo 1 > /sys/class/vtconsole/vtcon0/bind
# Some machines might have more than 1 virtual console. Add a line for each corresponding VTConsole
echo 1 > /sys/class/vtconsole/vtcon1/bind

nvidia-xconfig --query-gpu-info > /dev/null 2>&1
echo "efi-framebuffer.0" > /sys/bus/platform/drivers/efi-framebuffer/bind

# Restart Display Manager
systemctl start display-manager.service
