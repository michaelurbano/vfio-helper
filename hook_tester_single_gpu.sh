#!/bin/bash
# this program should be run with nohup, otherwise it will not function, as hook scripts log the user out
#DO NOT EDIT ANYTHING BELOW, IT COULD BREAK FUNCTIONALITY

#STARTSCRIPT
#REVERTSCRIPT

set -x

echo "Now testing your hook scripts for VM #####VMNAME"
echo "If your system decides to restart, then there is likely an issue with one of the hook scripts."
echo "Please reach out on GitHub, or check documentation here: https://github.com/joeknock90/Single-GPU-Passthrough?tab=readme-ov-file#common-issues."
echo "Or here: https://wiki.archlinux.org/title/PCI_passthrough_via_OVMF."

timeout 60 bash $START_SCRIPT

if [ $? -eq 124 ]; then
  echo "The script took longer than a minute to execute, we're restarting the system."
  echo "We'll run lsmod so you can see what drivers were still loaded."
  lsmod
  sudo reboot
else
  echo "The script executed within a minute. Continuing..."
fi

timeout 60 bash $REVERT_SCRIPT

if [ $? -eq 124 ]; then # 4096 is apparently the exit code for this?
  echo "The script took longer than a minute to execute, we're restarting the system."
  echo "We'll run lsmod so you can see what drivers were still loaded."
  lsmod
  sudo reboot
else
  echo "The script executed within a minute. Continuing..."
fi

echo "Assuming your display output has returned, it appears your scripts have executed successfully!"
echo "If that isn't the case, please raise an issue on the GitLab page, and include the hook_tester.log file as well."