import os
import sys
import subprocess
import platform
try:
    import libvirt
    _libvirtmodule = True
except:
    print("No libvirt module")
    _libvirtmodule = False
# set to True in order to bypass some of systemCheck().
_testing = False

### What's the intended difference between vfiohelper.py and vfioterminal.py?: ###

# vfiohelper.py is supposed to simply provide functions that will allow for the program to carry out tasks that it will need to do no matter what
# Essentially its just made so that creating a GUI application in the future will be a lot easier
# Unlike vfioterminal.py, it doesn't ever ask for inputs, at most it may print things, but that is it
# vfioterminal.py should only act more as a front-end, while vfiohelper.py is the backend
# At the moment, a lot more code is put into vfioterminal.py, but a lot of it will likely be moved to here


# These are system checks, they help with figuring out if the VMs will run on the PC
def systemCheck():
    print("Checking System Information...\n")
    ## Check if user is root ##
    if (os.getegid() == 0) and (not _testing):
        sys.exit("This program needs to NOT be run as root! You'll enter your sudo password later on.\nExiting...")
    try:
        osinfo = platform.freedesktop_os_release()
    except:
        rhel_based = subprocess.run(['cat', '/etc/os-release'], stdout=subprocess.PIPE, encoding='utf-8')
        if "rhel" in rhel_based.stdout:
            osinfo = {"ID":"rhel", "ID_LIKE":"rhel centos fedora"}
        else:
            osinfo = {"ID":"", "ID_LIKE":""}
    memBytes = os.sysconf('SC_PAGE_SIZE') * os.sysconf('SC_PHYS_PAGES')
    memMib = memBytes/(1024**2)
    cpuvendor = subprocess.run(['cat', '/proc/cpuinfo'], stdout=subprocess.PIPE, encoding='utf-8')
    vmxsvm = subprocess.run(['grep', '-Ec', "(vmx|svm)", '/proc/cpuinfo'], stdout=subprocess.PIPE, encoding='utf-8')
    iommu = subprocess.run(['journalctl', '-k'], stdout=subprocess.PIPE, encoding='utf-8')
    otheriommu = subprocess.run(['cat', '/proc/cmdline'], stdout=subprocess.PIPE, encoding='utf-8')
    kvmenabled = subprocess.run(['lsmod'], stdout=subprocess.PIPE, encoding='utf-8')

    if "linuxmint" in osinfo["ID"]:
        distro = "Ubuntu Derivative"
        print(f"Distribution\t{clr.mint}Linux Mint {clr.orange}(Ubuntu Derivative){clr.end}")
    elif "pop" in osinfo["ID"]:
        distro = "Ubuntu Derivative"
        print(f"Distribution\t{clr.brightcyan}Pop!_OS {clr.orange}(Ubuntu Derivative){clr.end}")
    elif "ubuntu" in osinfo["ID"]:
        distro = "Ubuntu"
        print(f"Distribution\t{clr.orange}{distro}{clr.end}")
    elif "debian" in osinfo["ID"]:
        distro = "Debian"
        print(f"Distribution\t{clr.brightred}{distro}{clr.end}")
    elif "fedora" in osinfo["ID"]:
        distro = "Fedora"
        print(f"Distribution\t{clr.blue}{distro}{clr.end}")
    elif "arch" in osinfo["ID"]:
        distro = "Arch Linux"
        print(f"Distribution\t{clr.brightblue}{distro}{clr.end}")

    elif "ubuntu" in osinfo["ID_LIKE"]:
        distro = "Ubuntu Derivative"
        print(f"Distribution\t{clr.orange}{distro}{clr.end}")
    elif "debian" in osinfo["ID_LIKE"]:
        distro = "Debian Derivative"
        print(f"Distribution\t{clr.brightred}{distro}{clr.end}")
    elif "fedora" in osinfo["ID_LIKE"]:
        distro = "Fedora Derivative"
        print(f"Distribution\t{clr.blue}{distro}{clr.end}")
    elif "arch" in osinfo["ID_LIKE"]:
        distro = "Arch Linux Derivative"
        print(f"Distribution\t{clr.brightblue}{distro}{clr.end}")

    else:
        distro = "Other"
        print(f"Distribution\t{clr.magenta}{distro}{clr.end}")
    if "AuthenticAMD" in cpuvendor.stdout:
        print(f"Vendor ID\t{clr.brightred}AuthenticAMD{clr.end}")
        vendor = "AMD"
    elif "GenuineIntel" in cpuvendor.stdout:
        print(f"Vendor ID\t{clr.brightblue}GenuineIntel{clr.end}")
        vendor = "Intel"
    else:
        print("Vendor ID\tUnknown")
        vendor = "Unknown"

    vcpus = int(vmxsvm.stdout)
    if vendor == "Intel":
        vcpus = vcpus//2
    print(f"vCPUs\t\t{clr.yellow}{vcpus}{clr.end}")
    if vmxsvm.stdout == "0\n" or (vcpus == 0):
        print(f"\n{clr.red}You need to enable virtualization in your BIOS/UEFI, check your motherboard documentation to see how.{clr.end}\n")
        boardname = subprocess.run(['cat', '/sys/class/dmi/id/board_name'], stdout=subprocess.PIPE, encoding='utf-8')
        boardversion = subprocess.run(['cat', '/sys/class/dmi/id/board_version'], stdout=subprocess.PIPE, encoding='utf-8')
        boardvendor = subprocess.run(['cat', '/sys/class/dmi/id/board_vendor'], stdout=subprocess.PIPE, encoding='utf-8')
        if not _testing:
            sys.exit(f"\nHere's board information that was found, try looking up BIOS/UEFI documentation in a search engine if you are unable to find the setting. {clr.yellow}/sys/class/dmi/id/:\n{boardname.stdout}Version: {boardversion.stdout}Vendor: {boardvendor.stdout}")

    print(f"Memory (MiB)\t{clr.yellow}{int(memMib)}{clr.end}")

    if 'iommu' in iommu.stdout:
        print(f"IOMMU\t\t{clr.green}Detected{clr.end}")
    else:
        boardname = subprocess.run(['cat', '/sys/class/dmi/id/board_name'], stdout=subprocess.PIPE, encoding='utf-8')
        boardversion = subprocess.run(['cat', '/sys/class/dmi/id/board_version'], stdout=subprocess.PIPE, encoding='utf-8')
        boardvendor = subprocess.run(['cat', '/sys/class/dmi/id/board_vendor'], stdout=subprocess.PIPE, encoding='utf-8')
        print(f"IOMMU\t\t{clr.red}Not Detected{clr.end}")
        print(f"\n{clr.red}You need to enable IOMMU in your BIOS/UEFI, check your motherboard documentation to see how. It's usually called AMD-Vi or Intel VT-d{clr.end}\n")
        if not _testing:
            sys.exit(f"\nHere's board information that was found, try looking up BIOS/UEFI documentation in a search engine if you are unable to find the setting. {clr.yellow}/sys/class/dmi/id/:\n{boardname.stdout}Version: {boardversion.stdout}Vendor: {boardvendor.stdout}")

    if vendor == "AMD":
        if 'amd_iommu=on' in otheriommu.stdout:
            print(f"amd_iommu=on\t{clr.green}Detected{clr.end}")
            iommuon = True
        else:
            print(f"amd_iommu=on\t{clr.red}Not Detected{clr.end}")
            iommuon = False
    elif vendor =="Intel":
        if 'intel_iommu=on' in otheriommu.stdout:
            print(f"intel_iommu=on\t{clr.green}Detected{clr.end}")
            iommuon = True
        else:
            print(f"intel_iommu=on\t{clr.red}Not Detected{clr.end}")
            iommuon = False
    else:
        if 'iommu=on' in otheriommu.stdout:
            print(f"iommu=on\t{clr.green}Detected{clr.end}")
            iommuon = True
        else:
            print(f"iommu=on\t{clr.red}Not Detected{clr.end}")
            iommuon = False

    if 'iommu=pt' in otheriommu.stdout:
        print(f"iommu=pt\t{clr.green}Detected{clr.end}")
        iommupt = True
    else:
        print(f"iommu=pt\t{clr.red}Not Detected{clr.end}")
        iommupt = False

    if 'video=efifb:off' in otheriommu.stdout:
        print(f"video=efifb:off\t{clr.green}Detected{clr.end}")
        efifboff = True
    else:
        print(f"video=efifb:off\t{clr.red}Not Detected{clr.end}")
        efifboff = False

    if 'kvm' in kvmenabled.stdout:
        print(f"KVM\t\t{clr.green}Detected{clr.end}")
        kvm = True
    else:
        print(f"KVM\t\t{clr.red}Not Detected{clr.end}")
        kvm = False

    graphicspresent = subprocess.run(['lspci', '-mm'], stdout=subprocess.PIPE, encoding='utf-8')
    gpulinesnv = []
    gpulinesamd = []
    gpulinesintel = []
    nvgpu = False
    amdgpu = False
    intelgpu = False
    nvgpucount = 0
    amdgpucount = 0
    intelgpucount = 0
    busNumber = ""
    increment = 0

    # Slightly more robust checking, it sees if the line contains the vendor name and VGA in "VGA compatible controller"
    # Then it checks if the lane after it shares the same bus number, if it does its added, if it doesnt, it checks for other devices
    # If exceptions come up, they should be addressed as some point
    print(f"\n{clr.brightgreen}NVIDIA{clr.end} GPU PCIe Addresses:{clr.end}")
    for line in graphicspresent.stdout.splitlines():
        if busNumber != "":
            tempLine = line
            if tempLine.split(":")[0] == busNumber:
                print(line)
                gpulinesnv[increment].append(line)
            else:
                increment += 1
                busNumber = ""

        if ("NVIDIA" in line) and ("VGA" in line):
            print(line)
            gpulinesnv.append([line])
            nvgpu = True
            busNumber = line
            busNumber = busNumber.split(":")[0]
    if nvgpu == False:
        print(f"{clr.red}None found{clr.end}\n")

    increment = 0
    print("\n")
    print(f"\n{clr.brightred}AMD{clr.end} GPU PCIe Addresses:")
    for line in graphicspresent.stdout.splitlines():
        if busNumber != "":
            tempLine = line
            if tempLine.split(":")[0] == busNumber:
                print(line)
                gpulinesamd[increment].append(line)
            else:
                increment += 1
                busNumber = ""

        if ("AMD" in line) and ("VGA" in line):
            print(line)
            gpulinesamd.append([line])
            amdgpu = True
            busNumber = line
            busNumber = busNumber.split(":")[0]
    if amdgpu == False:
        print(f"{clr.red}None found{clr.end}\n")

    increment = 0
    print("\n")
    print(f"{clr.brightblue}Intel{clr.end} GPU PCIe Addresses:")
    for line in graphicspresent.stdout.splitlines():
        if busNumber != "":
            tempLine = line
            if tempLine.split(":")[0] == busNumber:
                print(line)
                gpulinesintel[increment].append(line)
            else:
                increment += 1
                busNumber = ""

        if ("Intel" in line) and ("VGA" in line):
            print(line)
            gpulinesintel.append([line])
            intelgpu = True
            busNumber = line
            busNumber = busNumber.split(":")[0]
    if intelgpu == False:
        print(f"{clr.red}None found{clr.end}\n")

    increment = 0
    print("\n")
    print("Driver modules found currently running:")
    if "nouveau" in kvmenabled.stdout:
        print(f"{clr.green}nouveau{clr.end}")

    if "nvidia" in kvmenabled.stdout:
        print(f"{clr.brightgreen}nvidia{clr.end}")
        if "nvidia_modeset" in kvmenabled.stdout:
            print(f"{clr.brightgreen}nvidia_modeset{clr.end}")
        if "nvidia_uvm" in kvmenabled.stdout:
            print(f"{clr.brightgreen}nvidia_uvm{clr.end}")
        if "nvidia_drm" in kvmenabled.stdout:
            print(f"{clr.brightgreen}nvidia_drm{clr.end}")

    if "amdgpu" in kvmenabled.stdout:
        print(f"{clr.brightred}amdgpu{clr.end}")

    if 'i915' in kvmenabled.stdout:
        print(f"{clr.blue}i915{clr.end}")
    print("\n")
    print(f"{clr.green}All Done!{clr.end}\n")

    return (distro, vendor, vcpus, memMib, True, iommuon, iommupt, efifboff, kvm, gpulinesnv, gpulinesamd, gpulinesintel, nvgpu, amdgpu, intelgpu, nvgpucount, amdgpucount, intelgpucount)

# Returns True if there is an important argument missing from the grub configuration,
# Returns False if all the arguments are present, or if the CPU vendor is unknown
def grubCheck(vendor, iommuon, iommupt, efifboff):
    if False in (iommuon, iommupt, efifboff):
        if vendor == "Unknown":
            print("Your vendor is unknown? Good luck with that.")
            return False

        print("You need to change your Grub configuration in order to use a VM with VFIO. On the specified line, it is currently set to:\n")
        with open('/etc/default/grub', 'r') as file:
            grubcfg = file.readlines()
        for i, line in enumerate(grubcfg):
            if "GRUB_CMDLINE_LINUX_DEFAULT=" in line:
                print(f'{clr.red}'+str(i+1)+'\t'+str(line)+f'{clr.end}')
        print(f"Located at: {clr.brightyellow}/etc/default/grub{clr.end}")
        print("This needs to be changed, however, please be aware it carries a risk of rendering your system unbootable, if entered incorrectly")
        print("Here is an example configuration, change it to fit your needs:")
        print(f"\nGRUB_CMDLINE_LINUX_DEFAULT=\"splash {vendor.lower()}_iommu=on iommu=pt video=efifb:off\"\n")
        return True
    return False

# End of system checks

# Will add any missing arguments to grub so that passthrough can work properly
def grubFix(distro, vendor, iommuon, iommupt, efifboff):
    print("Now editing Grub configuration...")
    cwd = os.getcwd()
    with open('/etc/default/grub', 'r') as file:
        grubcfg = file.readlines()

    lineOfImportance = 0
    file = open(f'{cwd}/grubcfg', 'w')

    for i, line in enumerate(grubcfg):
        if ("GRUB_CMDLINE_LINUX_DEFAULT=" in line) and ("Fedora" not in distro):
            tempLine = line
            newLine = tempLine.split("\"")[0] + '\"' + tempLine.split("\"")[1]
            if not iommuon:
                newLine += f' {vendor.lower()}_iommu=on'
            if not iommupt:
                newLine += f' iommu=pt'
            if not efifboff:
                newLine += f' video=efifb:off'
            lineOfImportance = i+1
            print(newLine)
            file.writelines(newLine+'\"'+'\n')
            continue
        elif ("GRUB_CMDLINE_LINUX=" in line) and ("Fedora" in distro):
            tempLine = line
            newLine = tempLine.split("\"")[0] + '\"' + tempLine.split("\"")[1]
            if not iommuon:
                newLine += f' {vendor.lower()}_iommu=on'
            if not iommupt:
                newLine += f' iommu=pt'
            if not efifboff:
                newLine += f' video=efifb:off'
            lineOfImportance = i+1
            print(newLine)
            file.writelines(newLine+'\"'+'\n')
            continue
        file.writelines(line)
    file.close()

    with open(f'{cwd}/grubcfg', 'r') as file:
        grubcfg = file.readlines()
        for i, line in enumerate(grubcfg):
            if i+1 == lineOfImportance:
                print(f'{clr.brightyellow}' + str(i+1), f'{line}{clr.end}', end='')
                continue
            print(i+1,line, end='')

# Will actually perform the changes in the grubcfg file defined by grubFix
def grubChange(distro):
    cwd = os.getcwd()
    subprocess.run(['sudo', 'echo'])
    subprocess.run(['sudo', 'cp', '/etc/default/grub', '/etc/default/grub.bak'])
    subprocess.run(['sudo', 'cp', f'{cwd}/grubcfg', '/etc/default/grub'])
    if distro in ("Ubuntu", "Debian"):
        subprocess.run(['sudo', 'update-grub'])
    elif "Fedora" in distro:
        subprocess.run(['sudo', 'grub2-mkconfig', '-o', '/etc/grub2.cfg'])
    elif "Arch" in distro:
        print("Update grub, then restart your system.")

# For future use
def virtInstall():
    pass

# These are classes and functions meant for installing QEMU/KVM on different distributions
class DebianInstaller:
    def __init__(self):
        print(f"Performing actions for user:{clr.green}")
        os.system("echo $USER")
        print(f"{clr.end}\n")
    def update(self):
        subprocess.run(['apt', 'update'])
    def fullUpgrade(self):
        subprocess.run(['apt', 'full-upgrade'])
    def upgrade(self):
        subprocess.run(['apt', 'upgrade'])
    def installQEMUKVM(self):
        subprocess.run(['apt', 'install', 'qemu-kvm', 'libvirt-clients', 'libvirt-daemon-system', 'bridge-utils', 'libguestfs-tools', 'genisoimage', 'virtinst', 'libosinfo-bin', 'virt-manager', 'dnsmasq', 'iptables'])
        # Explanation of packages which include important tools:
        # libvirt-clients and libvirt-daemon-system: libvirt API for interacting with QEMU-KVM, brings libvirt shell virsh, and libvirt module in Python
        # virtinst: important for providing virt-install and virt-xml tools
    def installQEMUKVMDebian(self):
        subprocess.run(['apt', 'install', '--no-install-recommends', 'qemu-kvm', 'qemu-system-x86', 'libvirt-daemon-system', 'libvirt-clients', 'virt-manager', 'gir1.2-spiceclientgtk-3.0', 'dnsmasq', 'qemu-utils', 'iptables'])
    def addtogroup(self):
        os.system('sudo adduser $USER libvirt')
        os.system('sudo adduser $USER libvirt-qemu')
    def enableLibvirt(self):
        subprocess.run(['sudo', 'systemctl', 'enable', 'libvirtd'])
        subprocess.run(['sudo', 'systemctl', 'start', 'libvirtd'])

class FedoraInstaller:
    # There are probably functions missing to complete an install, I don't use Fedora so I couldn't tell you.
    def __init__(self):
        print("Performing actions for user:")
        os.system('echo $USER')
    def update(self):
        subprocess.run(['sudo', 'dnf', 'update'])
    def upgrade(self):
        subprocess.run(['sudo', 'dnf', 'upgrade'])
    def installQEMUKVM(self,withOptional=True):
        if withOptional == True:
            subprocess.run(['sudo', 'dnf', 'group', 'install', '--with-optional', 'virtualization'])
        else:
            subprocess.run(['sudo', 'dnf', 'install', '@virtualization'])
    def enableLibvirt(self):
        subprocess.run(['sudo', 'systemctl', 'enable', 'libvirtd'])
        subprocess.run(['sudo', 'systemctl', 'start', 'libvirtd'])

# Referred to instructions here: https://www.whonix.org/wiki/KVM#KVM_Setup_Instructions

# Ubuntu
def ubufun(update="y", fullupgrade="y", upgrade="n", install="y", setusergroup="y"):
    print("Proceeding with install...\n")
    debian = DebianInstaller()
    while True:
        isSudo = subprocess.run(['sudo', 'echo'], stderr=subprocess.PIPE, encoding='utf-8' )
        if isSudo.stderr == '':
            debian.update()
            debian.fullUpgrade()
            debian.installQEMUKVM()
            debian.addtogroup()
            debian.enableLibvirt()
            del debian
            break
        else:
            clearScreen()
            print(f"{clr.red}You failed the authentication, try again...{clr.end}\n")

# Debian
def debfun(update="y", fullupgrade="y", upgrade="n", install="y", setusergroup="y"):
    print("Make sure your current user is part of the sudoers group!")
    print("Proceeding with install...\n")
    debian = DebianInstaller()
    while True:
        isSudo = subprocess.run(['sudo', 'echo'], stderr=subprocess.PIPE, encoding='utf-8' )
        if isSudo.stderr == '':
            debian.update()
            debian.fullUpgrade()
            debian.installQEMUKVMDebian()
            del debian
            break
        else:
            clearScreen()
            print(f"{clr.red}You failed the authentication, try again...{clr.end}\n")

# Fedora/RHEL
def fedfun(update="y", upgrade="y", install="y", withOptional=True):
    print("Proceeding with install...\n")
    fedora = FedoraInstaller()
    while True:
        isSudo = subprocess.run(['sudo', 'echo'], stderr=subprocess.PIPE, encoding='utf-8' )
        if isSudo.stderr == '':
            fedora.update()
            fedora.upgrade()
            fedora.installQEMUKVM()
            fedora.enableLibvirt()
            del fedora
            break
        else:
            clearScreen()
            print(f"{clr.red}You failed the authentication, try again...{clr.end}\n")

# Arch Linux
def archfun():
    while True:
        isSudo = subprocess.run(['sudo', 'echo'], stderr=subprocess.PIPE, encoding='utf-8' )
        if isSudo.stderr == '':
            subprocess.run(['sudo', 'pacman', '-Syu', 'qemu', 'libvirt', 'virt-manager', 'qemu-arch-extra', 'dnsmasq', 'bridge-utils'])
            subprocess.run(['sudo', 'systemctl', 'enable', 'libvirtd'])
            subprocess.run(['sudo', 'systemctl', 'start', 'libvirtd'])
            break
        else:
            clearScreen()
            print(f"{clr.red}You failed the authentication, try again...{clr.end}\n")

# End of installers

# ANSI color escape sequences for coloring text in the terminal
class clr:
    '''
    Example: print({clr.yellow}"Hello"{clr.end})
    '''
    green = '\033[32m'
    yellow = '\033[33m'
    red = '\033[31m'
    blue = '\033[34m'
    cyan = '\033[36m'
    magenta = '\033[35m'
    brightgreen = '\033[92m'
    brightyellow = '\033[93m'
    brightred = '\033[91m'
    brightblue = '\033[94m'
    brightcyan = '\033[96m'
    brightmagenta = '\033[95m'
    end = '\033[0m'
    orange = '\033[38;5;208m'
    mint = '\033[38;5;121m'

# Self-explanatory, is for terminal
def clearScreen():
    subprocess.run(['clear'])
