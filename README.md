# Usage/Installation

#### Clone the repository:
```
git clone https://gitlab.com/michaelurbano/vfio-helper.git
cd vfio-helper
```
#### Then in a terminal, run:
`python3 vfioterminal.py`

#### Through releases:
1. Go to releases on the right.
2. Download Source code (tar.gz)
3. Uncompress tar.gz file.
4. In a terminal, go to the directory of the uncompressed file.
5. Type in the following to start the program:
`python3 vfioterminal.py`

# VFIO Helper

This is a project intended to help with setting up a high-performance Windows VM in Linux, with near native-performance to that of a bare-metal machine. This is done through the use of QEMU/KVM, and device passthrough with VFIO. At the moment, only Single-GPU passthrough setups are available, but in the future more setups will be supported.

## Dependencies/Requirements
* Python 3.10+
* Must use a distribution based on the following:
    * Ubuntu/Debian
    * Fedora/RHEL
    * Arch Linux
* systemd as init
* grub as bootloader

## Testing
This program has only ever been heavily tested on Linux Mint, with an AMD CPU and NVIDIA GPU. It should work on other Ubuntu or Debian distros fairly well.
It will probably work alright on Arch Linux.
It may have issues running on anything Fedora.

## To Do List
- [x] Make program function correctly on Fedora and related distributions.
- [x] Provide option to automatically edit grub configuration if it's not correct.
- [x] Check GPU driver in use on host machine.
- [x] Add option to test hook scripts, and if successful, attach GPU PCIe lanes to VM for single-GPU passthrough.
- [ ] Work on support for setups with multiple GPUs.
- [x] Provide releases as tar.gz.
- [ ] Provide releases as .deb files.
- [ ] Provide releases through other formats (rpm, AppImage).
- [ ] Create a GUI for application, once fully functional.
- [ ] Create a Wiki for documentation, and example setups.
- [ ] Perform a very large cleanup of the code.
- [ ] Automatic CPU Pinning
- [ ] Show available USB devices on Option 5
- [ ] Provide permissions to libvirt-qemu to access iso files in vfio-helper directory

## Other notes & Questions
Please raise an issue if any bugs or inconveniences are encountered with the program. I'll try to get back to them as soon as possible, thank you.

## Limitations
A select set of video-games with very aggressive anti-cheats will not properly function on these VMs, the only ones that come to mind:
* Anything using the Vanguard anticheat
    * Valorant
    * League of Legends
* Fortnite (when using the BattlEye anti-cheat)
Otherwise, other games should run without issues
#### Side note:
Please be aware that you could be banned from playing online if you do game on a VM. Game at your own risk

### Big thanks to these projects as well, this one wouldn't exist without them:

#### [Single-GPU-Passthrough](https://github.com/joeknock90/Single-GPU-Passthrough.git)

#### [VFIO-Tools](https://github.com/PassthroughPOST/VFIO-Tools.git)

### Questions asked
* Will a Flatpak version ever be made?
Probably not, because its sandboxing would just cause issues with installation.