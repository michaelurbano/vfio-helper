#!/bin/bash

virt-install \
--name singletest \
--os-variant win10 \
--memory 7952 \
--vcpus sockets=1,cores=8,threads=2 \
--cpu host-passthrough \
--disk bus=virtio,size=64 \
--cdrom /home/greenbean/Documents/repo/vfio-helper/Win10_22H2_English_x64v1.iso \
--disk path=/home/greenbean/Documents/repo/vfio-helper/virtio-win-0.1.248.iso,device=cdrom \
--network=default,model=virtio \
--boot hd,cdrom,uefi,bootmenu.enable=on \
--machine q35 \
--memballoon none \
--sysinfo host \
--features kvm.hidden.state=on
