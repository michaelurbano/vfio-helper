from vfiohelper import clr, systemCheck, virtInstall, grubCheck, grubFix, grubChange, ubufun, debfun, fedfun, archfun
import os
import sys
import subprocess
from pathlib import Path
import time
try:
    import libvirt
    _libvirtmodule = True
except:
    print("No libvirt module")
    _libvirtmodule = False
# Will bypass some of the checks performed, like for ISOs
_testing = False


## Part of Option 1
# Simply calls functions in vfiohelper.py for running apt update, or dnf update, etc. commands
def QEMUKVMInstaller(results):
    if "ubuntu" in results[0].lower():
        answer = confirmPrompt([f"Run the QEMU/KVM installer for {clr.orange}Ubuntu{clr.end}? It will:\n1. Update and Full Upgrade the system.\n2. Install necessary packages for QEMU/KVM and virt-manager.\n3. Add current user to necessary groups.\n4. Enable the libvirtd service\n\n"], "continue")
        if answer == True:
            clearScreen()
            ubufun()
        else:
            clearScreen()
            main()
    elif "debian" in results[0].lower():
        answer = confirmPrompt([f"Run the QEMU/KVM installer for {clr.red}Debian{clr.end}? It will:\n1. Update and Full Upgrade the system.\n2. Install necessary packages for QEMU/KVM and virt-manager.\n\nContinue? (y/N): "], "continue")
        if answer == True:
            clearScreen()
            debfun()
        else:
            clearScreen()
            main()
    elif "fedora" in results[0].lower():
        answer = confirmPrompt([f"Run the QEMU/KVM installer for {clr.blue}Fedora{clr.end}/{clr.red}RHEL{clr.end}? It will:\n1. Update and Upgrade the system.\n2. Install necessary packages for QEMU/KVM and virt-manager.\n3. Enable the libvirtd service\n\nContinue? (y/N): "], "continue")
        if answer == True:
            clearScreen()
            fedfun()
        else:
            clearScreen()
            main()
    elif "arch" in results[0].lower():
        answer = confirmPrompt([f"Run the QEMU/KVM installer for {clr.brightblue}Arch Linux{clr.end}? It will:\n1. Run pacman -Syu qemu libvirt virt-manager qemu-arch-extra dnsmasq bridge-utils\n2. sudo systemctl enable libvirtd\n(y/N): "], "continue")
        if answer == True:
            clearScreen()
            archfun()
        else:
            clearScreen()
            main()
    else:
        print("Your distribution couldn't be found, so we need you to do it manually. If your distribution isn't on this list, unfortunately, you're out of luck, and you'll need to manually install virtualization packages.\n\n1. Ubuntu\n2. Debian\n3. Fedora\n4. Arch Linux\n")
    
    clearScreen()
    print("Finished installing, please restart your system to ensure functionality")
    if "Fedora" in results[0]:
        print("There's also the possibility that the libvirtd configuration needs to be edited. If QEMU/KVM isn't working for you, try the following commands:")
        print(f"{clr.yellow}sudo nano /etc/libvirt/libvirtd.conf\nEdit the lines to the following:\nunix_sock_group = \"libvirt\"\nunix_sock_rw_perms = \"0770\"\nsudo usermod -a -G libvirt $(whoami){clr.end}")
        print("If you need to read these instructions again, just run the same commands as you did to install QEMU/KVM, it shouldn't affect your system, or visit https://www.whonix.org/wiki/KVM#Fedora to read full installation steps")
    main()

## Part of Option 2
# Will create a bash script of virt-install to setup a Windows VM, with some preferable changes to settings, depending on what the user inputs
def VMSetup(results):
    print(f"{clr.brightred}Ensure only one Windows ISO and one virtio-win ISO are present in the directory, and no others, otherwise the installation may have unintended side effects.\nDo not rename the ISOs.{clr.end}\n")
    print(f"Downloads can be found here:\nvirtio-win Latest ({clr.yellow}Recommended{clr.end}): https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/latest-virtio/virtio-win.iso")
    print(f"virtio-win Stable: https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/stable-virtio/virtio-win.iso")
    print(f"\nWindows 10 ({clr.yellow}Recommended{clr.end}): https://www.microsoft.com/en-us/software-download/windows10ISO")
    print(f"Windows 11 ({clr.red}Not Tested{clr.end}): https://www.microsoft.com/en-us/software-download/windows11")
    time.sleep(3)
    isolist = isoCheck()
    if (type(isolist) != list) and (not _testing):
        main()
    time.sleep(3)
    print("\nNow, a few questions in order to configure the VM to your liking...\n")
    while True:
        vmName = input(f"{clr.end}What would you like to name your VM? (It will be lowercased): {clr.yellow}").lower()
        if "y" in input(f"{clr.end}Are you sure you want to name your VM {clr.yellow}{vmName}{clr.end}?\n(y/n): ").lower():
            print(f"\n{clr.green}Continuing...{clr.end}\n")
            break
    while True:
        try:
            disksize = int(input(f"How many gibibytes should your OS disk be? ({clr.yellow}Minimum: 64 GiB {clr.end}({clr.blue}Windows 11{clr.end} Minimum requirement), {clr.green}Recommended: 128 GiB+{clr.end}): {clr.green}"))
            if disksize < 64:
                print(f"\n{clr.yellow}We highly advise selecting a bigger disk size.{clr.end}\n")
            else:
                print(f"\n{clr.green}Continuing...{clr.end}\n")
                break
        except ValueError:
            print(f"\n{clr.end}Please enter a valid number.\n")
    while True:
        hyperthread = input(f"{clr.end}Does your CPU have hyperthreading? (y/n): ")
        if "n" in hyperthread.lower():
            threads = 1
            print(f"\n{clr.green}Continuing...{clr.end}\n")
            break
        elif "y" in hyperthread.lower():
            threads = 2
            print(f"\n{clr.green}Continuing...{clr.end}\n")
            break

    time.sleep(1)
    print(f"\n{clr.yellow}Now, a bash script using virt-install will be created to help with installation.{clr.end}\n")
    # Lets iso be accessible otherwise install could fail
    subprocess.run(['sudo', 'chmod', '664', 'libvirt-qemu:kvm', f'{isolist[0]}'])

    file = open("virtinst.sh", "w")
    file.write("#!/bin/bash")
    file.write("\n")
    file.write("\n")
    file.write("virt-install \\")
    file.write("\n")
    file.write(f"--name {vmName} \\")
    file.write("\n")
    file.write("--os-variant win10 \\")
    file.write("\n")
    file.write(f"--memory {int(results[3]//2)} \\") # Maybe some rounding logic needs to be done for this, specifically to n^2 (4096, 8192, etc.)
    file.write("\n")
    file.write(f"--vcpus sockets=1,cores={results[2]//2},threads={threads} \\") # THIS needs to be changed so that it can accurately reflect the host CPU, but as long as the VM boots its good enough for now.
    file.write("\n")                                                            # lscpu -p might be the key here
    file.write("--cpu host-passthrough \\")                                     # Scratch that, virsh nodeinfo?
    file.write("\n")
    file.write(f"--disk bus=virtio,size={disksize} \\")
    file.write("\n")
    file.write(f"--cdrom {isolist[0]} \\")
    file.write("\n")
    file.write(f"--disk path={isolist[1]},device=cdrom \\")
    file.write("\n")
    file.write(f"--network=default,model=virtio \\")
    file.write("\n")
    file.write("--boot hd,cdrom,uefi,bootmenu.enable=on \\")
    file.write("\n")
    file.write("--machine q35 \\")
    file.write("\n")
    file.write("--memballoon none \\")
    file.write("\n")
    file.write("--sysinfo host \\")
    file.write("\n")
    file.write("--features kvm.hidden.state=on")
    file.write("\n")
    #file.write("--xml /domain/@xmlns:qemu=\"http://libvirt.org/schemas/domain/qemu/1.0\" --xml /domain/qemu:capabilities/qemu:del/@capability=usb-host.hostdevice")
    #file.write("\n") XML above is unused, it's intended to allow for passing through onboard bluetooth devices, but unfortunately, in order to do this, both of these XML edits would have to have been executed at the same time
    #file.write("--dry-run \\")
    #file.write("\n")
    #file.write("--debug") Debug info can be found  ~/.cache/virt-manager/virt-install.log regardless
    #file.write("\n")
    #file.write("--xml /domain/os/@firmware=efi") # Pre-QEMU/KVM 7.2.0 friendly solution to making sure that UEFI is used, virt-install has it though so not necessary
    #file.write("\n")
    file.close()
    file = open("virtinst.sh", "r")
    time.sleep(3)
    print(file.read())
    subprocess.run(['chmod', '+x', 'virtinst.sh'])
    time.sleep(1)
    while True:
        runScript = input(f"{clr.yellow}Do you want to run this script now in order to make the VM?\n(y/N): {clr.end}")
        if "y" in runScript.lower():
            break
        elif "n" in runScript.lower():
            print(f"\nSimply run {clr.yellow}./virtinst.sh{clr.end} in a terminal, in this directory, to run the installer.\n")
            print(f"{clr.red}If no bootable option or device was found:{clr.end}\n{clr.yellow}1. Press Enter\n2. Select Boot Manager\n3. Boot into the first DVD-ROM option.{clr.end}\n\n")
            print(f"{clr.yellow}When asked where you want to install Windows, and no disks appear:{clr.end}\n1. Press Load driver and OK\n2. Install the w10\\viostor.inf driveror (or w11\\viostor.inf if on Windows 11){clr.end}\n\n")
            print(f"{clr.yellow}Once Windows is fully installed, we highly recommened running the Virtio-win driver installer located at E:\\virtio-win-gt-x64{clr.end}\nThese drivers are needed to make internet connectivity possible through the virtio NIC\n\n")
            clearScreen()
            main()
    
    clearScreen()
    print(f"\nThe VM will now be installed. If you ever want to change the configuration, it can be done through Virtual Machine Manager\n{clr.yellow}Important information for install:{clr.end}\n\n")
    print(f"{clr.yellow}If no bootable option or device is found:{clr.end}\n1. Press Enter\n2. Select Boot Manager\n3. Boot into the first DVD-ROM option.{clr.end}\n\n")
    print(f"{clr.yellow}When asked where you want to install Windows, and no disks appear:{clr.end}\n1. Press Load driver and OK\n2. Install the amd64\\w10\\viostor.inf driver (or amd64\\w11\\viostor.inf if on Windows 11)\n\n")
    print(f"{clr.yellow}Once Windows is fully installed, we {clr.brightred}HIGHLY RECOMMEND{clr.yellow} running the Virtio-win driver installer located at E:\\virtio-win-gt-x64{clr.brightred}\nThese drivers are needed to make internet connectivity possible through the virtio NIC{clr.end}\n\n")

    time.sleep(5)
    subprocess.run(['bash', 'virtinst.sh'])
    print(f"\n{clr.yellow}If your VM has an internet connection, you should now run option 3.{clr.end}")
    main()

## Part of Option 2
# System check specific to the terminal, if a GUI version is made, the user can just enter the directories where they're stored, and make different checks.
def isoCheck():
    cwd = os.getcwd()
    pathcounter = 0
    pathlist = []
    print(f"\nChecking for ISOs in: {clr.yellow}{cwd}{clr.end}\n")
    for path in Path(f"{cwd}").glob("*.iso"):
        print(path)
        pathcounter += 1
        pathlist.append(str(path))
    if pathcounter > 2:
        print(f"\n{clr.red}Too many ISO files found{clr.end}")
    elif pathcounter < 2:
        print(f"\n{clr.red}Not enough ISO files found{clr.end}")
    elif pathcounter == 2:
        print(f"\n{clr.green}2 ISOs found!{clr.end}")
        # Lazy checks go on here
        windows = False
        virtio = False
        organizedpath = [None, None] # Windows path first, then virtio-win drivers afterwards
        for item in pathlist:
            if "virtio" in item.lower():
                virtio = True
                organizedpath[1] = item
            elif "win" in item.lower():
                windows = True
                organizedpath[0] = item
        if windows == True and virtio == True:
            print(f"\n{clr.green}ISOs with win and virtio in their names found, continuing with install...{clr.end}")
            return organizedpath
        else:
            print(f"{clr.red}ISOs with virtio-win or win in their name weren't detected, aborting the install...{clr.end}")
            main()

## Option 3
# This is responsible for creating and moving hook scripts to the VM, specifically for Single-GPU Passthrough
# Will call in vfiohelper.py to grubCheck if arguments in grub are missing, and grubFix if the user wants them to be automatically changed
def VFIOSetup(results):
    if grubCheck(results[1], results[5], results[6], results[7]):
        prompt1 = ["Would you like us to automatically change it?", f'{clr.brightred}WARNING: THIS COULD RENDER YOUR SYSTEM UNBOOTABLE, PLEASE DOUBLE CHECK CONFIGURATION BEFORE RESTARTING SYSTEM{clr.end}']
        answer = confirmPrompt(prompt1, "(y/n)")
        if answer:
            grubFix(results[0], results[1], results[5], results[6], results[7])
        else:
            menu()
        answer = confirmPrompt([f"Please ensure the {clr.brightyellow}yellow edited text{clr.end} is valid before editing grub."], "continue")
        if answer:
            grubChange(results[0])
            print("Now please restart your system for the changes to take effect.")
        else:
            menu()
    selection = 1
    if sum([results[15], results[16], results[17]]) >= 2:
        print(f"It's been detected you have {clr.yellow}2 or more GPUs{clr.end} on your system,\n but {clr.red}Traditional GPU passthrough, Dynamic GPU passthrough & Laptop GPU passthrough{clr.end} are not currently implemented, instead using Single-GPU Passthrough.")
    else:
        while True:
            answer = input(f"It's been detected you only have {clr.yellow}1 GPU{clr.end} on your system, continue with setup for a {clr.red}Single-GPU Passthrough{clr.end} configuration? \n(y/n): ")
            if 'y' in answer:
                break
            elif 'n' in answer:
                print("Returning to menu...")
                main()
    if selection == 1:
        print(f"\n{clr.green}Continuing with a Single GPU setup...{clr.end}\n")
        print("First, we'll need you to provide the name of the VM that we'll be editing.")
        while True:
            vmName = input("Name: ").lower()
            editVM = input(f"Are you sure you want to edit {vmName}?\n(y/n): ").lower()
            if 'y' in editVM.lower():
                break
            elif 'n' in editVM.lower():
                main()

        print("\n\nHook Helper from VFIO-Tools will now be installed at /etc/libvirt/hooks")
        while True:
            helper = input("Continue? (y/n): ").lower()
            if 'y' in helper.lower():
                os.system('sudo echo "Enter your sudo password: "')
                subprocess.run(['sudo', 'mkdir', '-p', '/etc/libvirt/hooks'])
                cwd = Path.cwd()
                subprocess.run(['sudo', 'cp', f'{cwd}/VFIO-Tools/libvirt_hooks/qemu', '/etc/libvirt/hooks/qemu'])
                subprocess.run(['sudo', 'chmod', '+x', '/etc/libvirt/hooks/qemu'])
                subprocess.run(['sudo', 'systemctl', 'restart', 'libvirtd'])
                break
            elif 'n' in helper.lower():
                print("Returning to menu...")
                main()
        
        print("\n\nNow, the PCIe lanes for your GPU will be put into hook scripts that will be activated for VM startup or shutdown.\n\n")
        # Redundant code as of right now, but it will have its uses later, I promise
        rawlanes = []
        if results[12] == True: # Checks NVIDIA GPU
            lanes = results[9][0]
            for i in lanes:
                rawlanes.append(i.split("\x20")[0])
        elif results[13] == True: # Checks AMD GPU
            lanes = results[10][0]
            for i in lanes:
                rawlanes.append(i.split("\x20")[0])
        elif results[14] == True: # Checks Intel GPU
            lanes = results[11][0]
            for i in lanes:
                rawlanes.append(i.split("\x20")[0])
        # Now to convert it to the nodedev format so we can put it into the hook scripts
        nodeDevs = []
        for i, v in enumerate(rawlanes):
            tempstring = v.replace(":", "_")
            nodeDevs.append(tempstring.replace(".", "_"))

        # NOW it needs to be put into the actual scripts, but directories need to be created first for both, and the start and revert scripts should be copied there
        subprocess.run(['sudo', 'mkdir', '-p', f'/etc/libvirt/hooks/qemu.d/{vmName}/prepare/begin'])
        subprocess.run(['sudo', 'mkdir', '-p', f'/etc/libvirt/hooks/qemu.d/{vmName}/release/end'])
        subprocess.run(['sudo', 'cp', f'{cwd}/Single-GPU-Passthrough/start.sh', f'/etc/libvirt/hooks/qemu.d/{vmName}/prepare/begin/start.sh'])
        subprocess.run(['sudo', 'cp', f'{cwd}/Single-GPU-Passthrough/revert.sh', f'/etc/libvirt/hooks/qemu.d/{vmName}/release/end/revert.sh'])
        subprocess.run(['sudo', 'chmod', '646', f'/etc/libvirt/hooks/qemu.d/{vmName}/prepare/begin/start.sh'])
        subprocess.run(['sudo', 'chmod', '646', f'/etc/libvirt/hooks/qemu.d/{vmName}/release/end/revert.sh'])
        subprocess.run(['sudo', 'cp', f'{cwd}/VFIO-Tools/libvirt_hooks/hooks/better_hugepages.sh', f'/etc/libvirt/hooks/qemu.d/{vmName}/prepare/begin/better_hugepages.sh'])
        subprocess.run(['sudo', 'chmod', '755', f'/etc/libvirt/hooks/qemu.d/{vmName}/prepare/begin/better_hugepages.sh'])
        # The scripts have 644 permissions when they're in their folders,
        # so they're going to be temporarily made into 646 so Python can have write permissions,
        # and then 755 for making them executable, but only after some checks
        
        laneNumber = 0
        with open(f'/etc/libvirt/hooks/qemu.d/{vmName}/prepare/begin/start.sh', 'r') as file:
            startScript = file.readlines()
        with open(f'/etc/libvirt/hooks/qemu.d/{vmName}/prepare/begin/start.sh', 'w') as file:
            for i, line in enumerate(startScript):
                if ("#REPLACE" in line) and (laneNumber <= len(nodeDevs)):
                    try:
                        file.writelines(f'virsh nodedev-detach pci_0000_{nodeDevs[laneNumber]}\n')
                        laneNumber += 1
                    except IndexError:
                        break
                else:
                    file.writelines(line)
        with open(f'/etc/libvirt/hooks/qemu.d/{vmName}/prepare/begin/start.sh', 'r') as file:
            newStartScript = file.readlines()
            for i, line in enumerate(newStartScript):
                print(str(i+1) +'\t'+line, end='')
        
        laneNumber -= 1

        with open(f'/etc/libvirt/hooks/qemu.d/{vmName}/release/end/revert.sh', 'r') as file:
            revertScript = file.readlines()
        with open(f'/etc/libvirt/hooks/qemu.d/{vmName}/release/end/revert.sh', 'w') as file:
            for i, line in enumerate(revertScript):
                if ("#REPLACE" in line) and (laneNumber >= 0):
                    file.writelines(f'virsh nodedev-reattach pci_0000_{nodeDevs[laneNumber]}\n')
                    laneNumber -= 1
                else:
                    file.writelines(line)
        with open(f'/etc/libvirt/hooks/qemu.d/{vmName}/release/end/revert.sh', 'r') as file:
            newRevertScript = file.readlines()
            for i, line in enumerate(newRevertScript):
                print(str(i+1) +'\t'+line, end='')
        
        print(f"\n{clr.brightyellow}Please check for errors that may be in the hook scripts. We would highly recommend testing the scripts with Option 4 before starting the VM.")
        print(f"Otherwise, trying to start the VM may just cause your system to \"hang\", requiring a restart.{clr.end}")
    main()

## Option 4
# This is responsible for checking that the scripts made in Option 3 function correctly by calling hook_tester_single_gpu.sh
def hookCheck(results):
    cwd = Path.cwd()
    while True:
        vmName = input("For which VM will you be testing the hook scripts for?: ").lower()
        if "y" in input(f"\n{clr.yellow}Are you sure you want to test {vmName}?\n(y/n): {clr.end}").lower():
            print(f"\n\n{clr.green}Continuing...{clr.green}\n\n")
            break
    print("To see if the hooks for a certain VM function properly, a tester script will be created to do just that.")
    print("It will need to be executed as root for it to work properly.")
    try:
        os.system('sudo echo')
    except PermissionError:
        print("You may have failed the authentication, please run this again to properly test the scripts")
        main()
    print(f"{clr.red}WARNING: THIS SCRIPT WILL EITHER LOG YOU OUT, OR RESTART YOUR PC.")
    print(f"PLEASE EXIT AND SAVE ANY WORK YOU CURRENTLY HAVE OPEN, THEN PRESS Y TO CONTINUE.{clr.end}")
    while True:
        keepGoing = input("CONTINUE (Y/n)?: ").lower()
        if "y" in keepGoing:
            secondPrompt = input(f"\nAre you absolutely sure you want to test hook scripts for {vmName}?\n(y/n): ").lower()
            if "y" in secondPrompt:
                print("Continuing...")
                break
            elif "n" in secondPrompt:
                print("Returning to the menu...")
                main()
        elif "n" in keepGoing:
            print("Returning to the menu...")
            main()
    subprocess.run(['sudo', 'chmod', '755', f'/etc/libvirt/hooks/qemu.d/{vmName}/prepare/begin/start.sh'])
    subprocess.run(['sudo', 'chmod', '755', f'/etc/libvirt/hooks/qemu.d/{vmName}/release/end/revert.sh'])
    subprocess.run(['cp', f'{cwd}/hook_tester_single_gpu.sh', f'{cwd}/{vmName}_hook_tester_single_gpu.sh'])
    subprocess.run(['sudo', 'chmod', '+x', f'{cwd}/{vmName}_hook_tester_single_gpu.sh'])
    with open(f'{cwd}/{vmName}_hook_tester_single_gpu.sh', 'r') as file:
        editor = file.readlines()
    with open(f'{cwd}/{vmName}_hook_tester_single_gpu.sh', 'w') as file:
        for i, line in enumerate(editor):
            if ("#STARTSCRIPT" in line):
                file.writelines(f'START_SCRIPT=/etc/libvirt/hooks/qemu.d/{vmName}/prepare/begin/start.sh\n')
            elif "#REVERTSCRIPT" in line:
                file.writelines(f'REVERT_SCRIPT=/etc/libvirt/hooks/qemu.d/{vmName}/release/end/revert.sh\n')
            else:
                file.writelines(line)
                
    print(f"\nWe will now test the hook scripts for {vmName}, see you on the other side!\n")
    print(f"{clr.brightred}IF YOUR DISPLAY DOES NOT RETURN WITHIN 3 MINUTES, THE SCRIPTS LIKELY DID NOT WORK. RUN THIS IN ORDER TO SAFELY RESTART YOUR SYSTEM:{clr.end}")
    print(f"{clr.brightred}Alt + PrtSc (Then let go of PrtSc, keep holding Alt, now type the letters) R E I S U B{clr.end}\n")
    print("\nExecuting...")
    time.sleep(10)
    os.system(f'sudo nohup {cwd}/{vmName}_hook_tester_single_gpu.sh > hook_tester.log')

## Option 5
# Essentially the final step for Single-GPU passthrough. It will add the PCIe lanes to the given VM so that it may be passed through
def attachLanes(results):
    print("Now it's time to attach the PCIe lanes to your VM, and tweak a few settings.\n")
    cwd = Path.cwd()
    while True:
        vmName = input("\nWhich VM will you be editing?: ").lower()
        if "y" in input(f"Are you sure you want to edit {vmName}?\n(y/n): ").lower():
            print("\n\nContinuing...")
            break

    # Attaching lanes here
    rawlanes = []
    if results[12] == True: # Checks NVIDIA GPU
        lanes = results[9][0]
        for i in lanes:
            rawlanes.append(i.split("\x20")[0])
    elif results[13] == True: # Checks AMD GPU
        lanes = results[10][0]
        for i in lanes:
            rawlanes.append(i.split("\x20")[0])
    elif results[14] == True: # Checks Intel GPU
        lanes = results[11][0]
        for i in lanes:
            rawlanes.append(i.split("\x20")[0])
    # To nodedev format
    nodeDevs = []
    for i, v in enumerate(rawlanes):
        tempstring = v.replace(":", "_")
        nodeDevs.append(tempstring.replace(".", "_"))
    for pci in nodeDevs:
        subprocess.run(['virt-xml', f'{vmName}', '--add-device', '--hostdev', f'pci_0000_{pci}'])
    
    subprocess.run(['virt-xml', f'{vmName}', '--remove-device', '--graphics', 'all'])
    subprocess.run(['virt-xml', f'{vmName}', '--remove-device', '--video', 'all'])

    print("\nOkay, your VM should be mostly good to go.")
    print("These are the last steps you'll need to perform on your own before you try and start the VM:\n")
    print("Please add the USB devices for your keyboard and mouse to the VM, this can be done through virt-manager with the following steps:")

    print(f"{clr.brightyellow}1. Double click the VM you want to edit.{clr.end}")
    print(f"{clr.brightyellow}2. Press button next to the Power On button for the VM.{clr.end}")
    print(f"{clr.brightyellow}3. Select Add Hardware at the bottom left.{clr.end}")
    print(f"{clr.brightyellow}4. Select USB Host Device.{clr.end}")
    print(f"{clr.brightyellow}5. Select the device which is your mouse/keyboard, and press finish.{clr.end}")
    print(f"{clr.brightyellow}Make sure to add both your keyboard and mouse!{clr.end}")

    print(f"\n{clr.brightyellow}Please wait 10-20 minutes so that the display drivers can automatically install, assuming your VM is connected to the internet.{clr.end}")
    print("Otherwise, if you know what you are doing, you can add a VNC display to more quickly download display drivers in Windows.")
    main()

## Option 6
# This optionally allows for a user to add a VNC display to a VM in order to speed up installing drivers for Single-GPU passthrough systems
def addVNC(results):
    print("This is an optional step which allows for adding a VNC display to your VM, it will let you more quickly download drivers on a Single-GPU setup.")
    print("Otherwise, you have to wait for 10-20 minutes for the drivers to automatically download.")
    print(f"{clr.brightred}We would only recommend turning this on if you fully understand the security implications of this.{clr.end}")
    print("This VNC server can be removed by running option 7.")
    answer = confirmPrompt(['Continue or return to menu?'], 'continue')
    if answer == True:
        vmName = confirmPrompt([''], 'vmName')
        print(f"Editing XML for {vmName}")
        subprocess.run(['virt-xml', f'{vmName}', '--add-device', '--graphics', 'vnc', '--confirm'])
        subprocess.run(['virt-xml', f'{vmName}', '--add-device', '--video', 'qxl', '--confirm'])

    else:
        main()

## Option 7
# This optionally allows for a user to remove the VNC display from option 6, as well as all other display and graphics devices for the VM
def removeVNC(results):
    print("This option will remove a VNC displays, or any other virtual displays or virtual graphics that may be connected to a VM")
    answer = confirmPrompt(['Continue or return to menu?'], 'continue')
    if answer == True:
        vmName = confirmPrompt([''], 'vmName')
        print(f"Editing XML for {vmName}")
        subprocess.run(['virt-xml', f'{vmName}', '--remove-device', '--graphics', 'all', '--confirm'])
        subprocess.run(['virt-xml', f'{vmName}', '--remove-device', '--video', 'all', '--confirm'])
    else:
        main()

## Option 8
# Will show whether or not hook_tester.log file was able to successfully execute
def checkHookTesterLog(results):
    pass
### END OF OPTION FUNCTIONS ###

# I got really tired of having to write a (y/n) prompt every time I needed user confirmation, so I made a chatterbox function to fix that
# It takes your list of strings, prompt, then a promptType, which is defined for each match case
# The color can be changed to any clr.attribute, and if you make endColor = False, the words you type will be colored the same too
# For selection options requiring the user to select a number, you can specify in numberRange ex: [1,8] specifies a number choice 1 through 8, inclusive
# It will return True or False for yes or no prompts, strings for things like names, and numbers for number choices.
# EXAMPLE USE: confirmPrompt(["Do you want to install Libvirt?", "It only takes a few seconds."], "(y/n)")
# EXAMPLE OUTPUT:
# Do you want to install Libvirt?
# It only takes a few seconds.
# (y/n):
def confirmPrompt(prompt, promptType, numberRange=[0,0]):
    for i in prompt:
        print(i)

    match promptType:

        case "continue":
            while True:
                answer = input(f"{clr.end}Continue? (y/n): {clr.end}")
                print(f"{clr.end}")
                if 'y' == answer:
                    return True
                elif 'n' == answer:
                    return False
        
        case "(y/n)":
            while True:
                answer = input(f"{clr.end}(y/n)?: {clr.end}")
                print(f"{clr.end}")
                if 'y' == answer:
                    return True
                elif 'n' == answer:
                    return False

        case "vmName":
            while True:
                answer = input(f"{clr.end}Please enter the name of your VM: {clr.end}")
                print(f"{clr.end}")
                answerTwo = input(f"Are you sure you want to edit {answer.lower()}?")
                if answerTwo == 'y':
                    return answer.lower()
                elif answerTwo == 'n':
                    print("Go back to the menu?")
                    while True:
                        answerTwo = input(f"(y/n): ")
                        if answerTwo == 'y':
                            return "main()"
                        elif answerTwo == 'n':
                            break

        case "number":
            while True:
                try:
                    answer = int(input(f"{clr.end}Please enter a number {numberRange[0]} through {numberRange[1]}: {clr.end}"))
                    print(f"{clr.end}")
                    if numberRange[0] <= answer >= numberRange[1]:
                        return answer
                    print("Please enter a valid number option.")
                except:
                    print("Please enter a valid option.")

def clearScreen():
    subprocess.run(['clear'])

#### START OF PROGRAM ####

print(f"{clr.brightgreen}Hello! One moment please...{clr.end}")
results = systemCheck()
'''
(I should've used a dictionary for all this, but I didn't think of it at the time, sorry)
Heres a key for parsing through the tuple returned by systemCheck():

results[0]: Linux distribution, or distribution derivative | str
results[1]: CPU vendor | str
results[2]: vCPU count, or how many times VMX/SVM was found in /proc/cpuinfo with grep (Always more than 0, otherwise sys.exit()) | int
results[3]: Total system memory, in mebibytes | int
results[4]: IOMMU enabled (Always True, otherwise sys.exit()) | bool
results[5]: amd_iommu=on/intel_iommu=on in grub configuration | bool
results[6]: iommu=pt in grub configuration | bool
results[7]: video=efifb:off in grub configuration | bool
results[8]: KVM enabled | bool
results[9][i]: list of PCIe lanes that are used by an NVIDIA GPU | list[list[str]]
results[10][i]: list of PCIe lanes that are used by an AMD GPU | list[list[str]]
results[11][i]: list of PCIe lanes that are used by an Intel GPU | list[list[str]]
results[12]: Is there an NVIDIA GPU on the system? | bool
results[13]: Is there an AMD GPU on the system? | bool
results[14]: Is there an Intel GPU on the system? | bool
results[15]: How many NVIDIA GPUs are on the system? | int
results[16]: How many AMD GPUs are on the system? | int
results[17]: How many an Intel GPUs are on the system? | int
'''

def main():
    print(f"\n{clr.brightgreen}Welcome to VFIO Helper!{clr.end}")
    #print(f"Type {clr.green}M{clr.end} in order to return to the menu at any time.\n\n")
    print(f"\nSelect what you would like to do:\n1. Install QEMU/KVM and Virt-Manager {clr.brightred}(REQUIRES RESTART){clr.end}\n2. Create a Windows VM with recommended settings\n3. Set up VFIO hook scripts for a VM {clr.brightred}(ONLY SINGLE GPU SUPPORT){clr.end}\n4. Test hook scripts for VM {clr.brightred}(ONLY SINGLE GPU SUPPORT){clr.end}\n5. Attach PCIe lanes to VM {clr.brightred}(ONLY SINGLE GPU SUPPORT){clr.end}")
    print(f"6. Add a VNC display to a VM {clr.brightyellow}\"advanced option\"{clr.end}\n7. Remove VNC display, and other virtual displays/graphics devices {clr.brightyellow}\"advanced option\"{clr.end}\n8. View hook tester results\n9. Run system check again")
    print("10. Exit")

    while True:
        try:
            selection = int(input("Select a number 1-10: "))
            if selection <= 10:
                break
        except ValueError:
            print("Please enter a valid number.")

    if selection == 1:
        clearScreen()
        QEMUKVMInstaller(results)
    elif selection == 2:
        clearScreen()
        VMSetup(results)
    elif selection == 3:
        clearScreen()
        VFIOSetup(results)
    elif selection == 4:
        clearScreen()
        hookCheck(results)
    elif selection == 5:
        clearScreen()
        attachLanes(results)
    elif selection == 6:
        clearScreen()
        addVNC(results)
    elif selection == 7:
        clearScreen()
        removeVNC(results)
    elif selection == 8:
        clearScreen()
        checkHookTesterLog()
    elif selection == 9:
        clearScreen()
        systemCheck()
        main()
    elif selection == 10:
        sys.exit("bye...")

if __name__ == "__main__":
    main()